import * as vscode from 'vscode'
import { TestSuiteInfo, TestInfo, TestRunStartedEvent, TestRunFinishedEvent, TestSuiteEvent, TestDecoration, TestEvent } from 'vscode-test-adapter-api'
import { Log } from 'vscode-test-adapter-util'
import { Package, File, Symbol, SymbolType, Lookup } from './model'
import { Configuration } from './config'

class Base<Test, Model extends { name: string | null }> {
    protected readonly byName: Lookup<Test> = {}

    constructor(
        private readonly ctor: { new(m: Model): Test }
    ) { }

    get children() { return Object.keys(this.byName).map(x => this.byName[x]) }

    find(name: string): Test | null {
        return this.byName[name]
    }

    get(model: Model): Test {
        if (model.name! in this.byName)
            return this.byName[model.name!]

        const test = new this.ctor(model)
        this.byName[model.name!] = test
        return test
    }
}

export class TestCollector extends Base<TestPackage, Package> {
    public get packages(): Lookup<TestPackage> { return this.byName }

    constructor() { super(TestPackage) }

    async add(file: File, retire?: Set<TestSymbol>, retireWithoutChanges = false) {
        const tests = this.get(file.pkg).get(file)
        const existing = new Set(Object.keys(tests.symbols))

        for (const sym of await file.symbols()) {
            existing.delete(sym.name)

            const test = tests.get(sym)
            if (retireWithoutChanges)
                retire?.add(test)

            if (sym === test.symbol)
                continue

            if (test.symbol.text == sym.text && test.symbol.info.location.range.start.line == sym.info.location.range.start.line)
                continue

            retire?.add(test)
            test.symbol = sym
        }

        for (const name of existing) {
            const test = tests.find(name)!
            delete tests.symbols[name]
            retire?.add(test)
        }
    }

    toInfo(label: string): TestSuiteInfo {
        return {
            type: 'suite',
            id: 'Go',
            label,
            children: this.children
                .sort((a, b) => a.pkg.name.localeCompare(b.pkg.name))
                .map(x => x.toInfo()),
        }
    }
}

export class TestPackage extends Base<TestFile, File> {
    public readonly type = TestType.Package;

    public get id() { return this.pkg.id }
    public get files(): Lookup<TestFile> { return this.byName }

    constructor(
        public readonly pkg: Package,
    ) { super(TestFile) }

    findSymbol(name: string): TestSymbol | null {
        for (const file of this.children) {
            const sym = file.find(name)
            if (sym) return sym
        }
        return null
    }

    toInfo(): TestSuiteInfo {
        const suite: TestSuiteInfo = {
            type: 'suite',
            id: this.pkg.id,
            label: this.pkg.name,
            children: [],
        }

        for (const file of this.children.sort((a, b) => a.file.name.localeCompare(b.file.name))) {
            const info = file.toInfo()
            if (Configuration.get(file.file.uri).showFiles != 'always')
                suite.children.push(...info.children)
            else if (info.children.length)
                suite.children.push(info)
        }

        return suite
    }
}

export class TestFile extends Base<TestSymbol, Symbol> {
    public readonly type = TestType.File;

    public get id() { return this.file.id }
    public get symbols(): Lookup<TestSymbol> { return this.byName }

    constructor(
        public readonly file: File,
    ) { super(TestSymbol) }

    toInfo(): TestSuiteInfo {
        return {
            type: 'suite',
            id: this.file.id,
            label: this.file.name,
            file: uriToInfoPath(this.file.uri),
            children: this.children
                .map(x => x.toInfo())
                .sort((a, b) => a.line! - b.line!),
        }
    }
}

export class TestSymbol {
    public get type() {
        switch (this.symbol.type) {
        case SymbolType.Test:
            return TestType.Test
        case SymbolType.Example:
            return TestType.Example
        case SymbolType.Benchmark:
            return TestType.Benchmark
        default:
            return TestType.Unknown
        }
    }

    public get id() { return this.symbol.id }

    constructor(
        public symbol: Symbol,
    ) { }

    toInfo(): TestInfo {
        return {
            type: 'test',
            id: this.symbol.id,
            label: this.symbol.name.replace(/([a-z0-9][A-Z]|[a-zA-Z0-9]_[a-zA-Z0-9])/g, (_, s) => `${s[0]} ${s[s.length-1].toLowerCase()}`),
            file: uriToInfoPath(this.symbol.file.uri),
            line: this.symbol.info.location.range.start.line,
        }
    }
}

export enum TestType {
    Unknown,
    Package,
    File,
    Test,
    Example,
    Benchmark
}

type TestOrExampleSymbol = TestSymbol & {
    type: TestType.Test | TestType.Example
}

type BenchmarkSymbol = TestSymbol & {
    type: TestType.Benchmark
}

type TestInfoState = 'running' | 'passed' | 'failed' | 'skipped' | 'errored'

export class TestRun {
    public readonly suites = new Set<TestPackage | TestFile>()
    public readonly pkg: Package
    public tests: TestPackage | Set<TestOrExampleSymbol> = new Set()
    public benchmarks = new Set<BenchmarkSymbol>()

    private output = new Map<Package | Symbol, string[]>()
    private status: Lookup<TestInfoState> = {};

    constructor(
        testPkg: TestPackage,
        public readonly log: Log,
        public readonly emitter: vscode.EventEmitter<TestRunStartedEvent | TestRunFinishedEvent | TestSuiteEvent | TestEvent>,
    ) {
        this.pkg = testPkg.pkg
    }

    add(test: TestSymbol) {
        if (this.tests instanceof TestPackage)
            return

        switch (test.type) {
        case TestType.Test:
        case TestType.Example:
            this.tests.add(<TestOrExampleSymbol>test)
            break

        case TestType.Benchmark:
            this.benchmarks.add(<BenchmarkSymbol>test)
            break
        }
    }

    get hasTests() {
        if (this.tests instanceof Set)
            return this.tests.size > 0
        return this.tests.children.some(x => x.children.some(x => x.type == TestType.Test || x.type == TestType.Example))
    }

    get hasBenchmarks() {
        if (this.tests instanceof Set)
            return this.benchmarks.size > 0
        return this.tests.children.some(x => x.children.some(x => x.type == TestType.Benchmark))
    }

    get all(): TestSymbol[] {
        if (this.tests instanceof Set)
            return [...this.tests, ...this.benchmarks]

        const all: TestSymbol[] = []
        this.tests.children.forEach(x => all.push(...x.children))
        return all
    }

    *allBenchmarks(): Generator<BenchmarkSymbol, void, undefined> {
        if (this.tests instanceof Set) {
            yield* this.benchmarks
            return
        }

        for (const file of this.tests.children)
            for (const sym of file.children)
                if (sym.type == TestType.Benchmark)
                    yield <BenchmarkSymbol>sym
    }

    lastEventLine: string = '';

    event(e: GoTestEvent) {
        if (this.lastEventLine) {
            e.Output = this.lastEventLine + e.Output
        }

        if (!/^\s*Benchmark\w+/.test(e.Output)) {
            const test = this.getTest(e)
            if (!test || e.Action == 'output' && this.isDone(test.id))
                this.packageEvent(e)
            else
                this.testEvent(e)
            return
        }

        const i = e.Output.indexOf('\n')
        if (i < 0) {
            this.lastEventLine = e.Output
            return
        }

        this.lastEventLine = e.Output.slice(i+1)
        e.Output = e.Output.slice(0, i)
        this.benchmarkEvent(e)
    }

    isDone(id: string): false | 'passed' | 'skipped' | 'failed' | 'errored' {
        if (!(id in this.status)) return false
        const status = this.status[id]
        if (status == 'running') return false
        return status
    }

    private record(m: Package | Symbol, l: string) {
        if (this.output.has(m))
            this.output.get(m)!.push(l)
        else
            this.output.set(m, [l])
    }

    private packageEvent(e: GoTestEvent) {
        const type = 'suite'
        const suite = this.pkg.id

        switch (e.Action) {
        case 'run':
            this.log.info(`${this.pkg.path} running`)
            this.fire<TestSuiteEvent>('test suite event', { type, suite, state: 'running' })
            break

        case 'pass':
        case 'fail':
            this.log.info(`${this.pkg.path} completed`)
            this.fire<TestSuiteEvent>('test suite event', { type, suite, state: 'completed' })
            break

        case 'output':
            this.log.info(`${this.pkg.path} => ${e.Output.trimRight()}`)
            this.record(this.pkg, e.Output)
            return

        default:
            this.log.debug(`Ignored event for ${this.pkg.path}`, e)
            break
        }
    }

    private getBench(e: GoTestEvent) {
        if (!e.Output) return
        const symName = e.Output.replace(/^\s*(Benchmark\w+).*$/, '$1')
        return this.tests instanceof Set ? [...this.benchmarks].find(x => x.symbol.name == symName) : this.tests.findSymbol(symName)
    }

    private getTest(e: GoTestEvent) {
        if (!e.Test) return
        const symName = e.Test.replace(/\/.*$/, '')
        return this.tests instanceof Set ? [...this.tests].filter(x => x.symbol.name == symName)[0] : this.tests.findSymbol(symName)
    }

    private benchmarkEvent(e: GoTestEvent) {
        const bench = this.getBench(e)
        if (bench == null) {
            this.packageEvent(e)
            return
        }

        switch (e.Action) {
        case 'output':
            this.log.info(`${this.pkg.path}·${bench.symbol.name} => ${e.Output.trimRight()}`)
            this.record(bench.symbol, e.Output)
            return

        default:
            this.log.debug(`Ignored event for ${this.pkg.path}·${bench.symbol.name}`, e)
            break
        }
    }

    private testEvent(e: GoTestEvent) {
        const test = this.getTest(e)
        if (test == null) {
            this.packageEvent(e)
            return
        }

        let state: TestInfoState
        switch (e.Action) {
        case 'run':
            state = 'running'
            this.status[test.id] = state
            this.log.info(`${this.pkg.path}·${test.symbol.name} started`)
            return

        case 'pass':
            state = 'passed'
            break

        case 'fail':
            state = 'failed'
            break

        case 'skip':
            state = 'skipped'
            break

        case 'output':
            // FIX: https://gitlab.com/firelizzard/vscode-go-test-adapter/-/issues/1
            if (e.Output.startsWith('--- FAIL:'))
                this.status[test.id] = 'failed'
            else if (e.Output.startsWith('--- PASS:'))
                this.status[test.id] = 'passed'

            this.log.info(`${this.pkg.path}·${test.symbol.name} => ${e.Output.trimRight()}`)
            this.record(test.symbol, e.Output)
            return

        default: // pause, cont, bench
            this.log.debug(`Ignored event for ${this.pkg.path}·${test.symbol.name}`, e)
            return
        }

        // FIX: https://gitlab.com/firelizzard/vscode-go-test-adapter/-/issues/1
        if (state == 'passed' && this.isDone(test.id))
            state = this.status[test.id]
        else
            this.status[test.id] = state

        this.log.info(`${this.pkg.path}·${test.symbol.name} ${state}`)

        const output = this.output.get(test.symbol) || []
        const message = output.join('')
        const parsed = new Map<number, { all: string, error?: string }>()
        let current: { all: string, error?: string } | undefined
        for (const line of output) {
            const fileAndLine = line.match(/^\s*(?<file>.*_test\.go):(?<line>\d+): ?(?<message>.*\n)$/)
            if (fileAndLine) {
                current = { all: fileAndLine.groups!.message }
                parsed.set(Number(fileAndLine.groups!.line), current)
                continue
            }

            if (!current)
                continue

            const entry = line.match(/^\s*(?:(?<name>[^:]+): *| +)\t(?<message>.*\n)$/)
            if (!entry)
                continue

            current.all += entry.groups!.message
            if (entry.groups!.name == 'Error') {
                current.error = entry.groups!.message
            } else if (!entry.groups!.name && current.error)
                current.error += entry.groups!.message
        }

        const decorations: TestDecoration[] = Array.from(parsed.keys()).map(line => {
            const { all, error } = parsed.get(line)!
            const hover = (error || all).trim()
            const message = hover.split('\n')[0].replace(/:\s+$/, '')

            // for some reason, the test explorer extension puts decorations on the wrong line
            return { line: line - 1, message, hover }
        })

        this.fireEvent(test, { state, message, decorations })
    }

    fire<T extends TestSuiteEvent | TestEvent>(description: string, event: T) {
        this.log.debug(`Firing ${description}`, event)
        this.emitter.fire(event)
    }

    fireEvent(test: TestSymbol, event: Omit<TestEvent, 'type' | 'test'>) {
        this.log.debug(`Firing test event for ${test.id.slice(3)}`, event)
        this.emitter.fire(Object.assign({ type: <'test'>'test', test: test.id }, event))
    }

    fireSuite(suite: TestPackage | TestFile, event: Omit<TestSuiteEvent, 'type' | 'suite'>) {
        this.log.debug(`Firing test suite event for ${suite.id.slice(3)}`, event)
        this.emitter.fire(Object.assign({ type: <'suite'>'suite', suite: suite.id }, event))
    }

    fireAllSuites(event: Omit<TestSuiteEvent, 'type' | 'suite'>) {
        this.log.debug(`Firing test suite event for all suites`, event, [...this.suites].map(x => x.id.slice(3)))
        for (const suite of this.suites)
            this.emitter.fire(Object.assign({ type: <'suite'>'suite', suite: suite.id }, event))
    }

    fireBenchmarks(state: 'running' | 'errored' | 'failed' | 'passed', message?: string) {
        for (const bench of this.allBenchmarks()) {
            this.status[bench.id] = state
            let m = message
            if (!m && state != 'running')
                m = this.output.get(bench.symbol)?.join('\n')
            this.fire<TestEvent>('benchmark event', { type: 'test', test: bench.id, state, message: m })
        }
    }
}

export interface GoTestEvent {
    Time: string
    Action: 'run' | 'pause' | 'cont' | 'pass' | 'bench' | 'fail' | 'output' | 'skip'
    Package: string
    Test: string
    Elapsed: number
    Output: string
}

function uriToInfoPath(uri: vscode.Uri): string {
    return uri.scheme == 'file' ? uri.fsPath : uri.toString()
}