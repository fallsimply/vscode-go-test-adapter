import vscode = require('vscode');
import os = require('os');
import path = require('path');
import fs = require('fs');
import { spawn, exec, ChildProcess } from 'child_process';
import { Transform, TransformCallback } from 'stream';
import { StringDecoder } from 'string_decoder';
import { TestAdapter, TestLoadStartedEvent, TestLoadFinishedEvent, TestRunStartedEvent, TestRunFinishedEvent, TestSuiteEvent, TestEvent, RetireEvent } from 'vscode-test-adapter-api';
import { Log } from 'vscode-test-adapter-util';
import { Module, File, Lookup } from './model';
import { TestCollector, TestSymbol, TestPackage, TestRun, TestType } from './tests';
import { Configuration } from './config';
import { getBinPath, getGoConfig, getTestEnvVars, getTestFlags, getTestTags } from './vscodeGo';

const fsp = fs.promises;

class LineTransform extends Transform {
    private last = ''
    private decoder = new StringDecoder('utf8')

    constructor() {
        super({
            objectMode: true,

            transform(this: LineTransform, chunk: any, encoding: string, callback: TransformCallback) {
                this.last += this.decoder.write(chunk)
                const list = this.last.split('\n')
                this.last = list.pop()!

                for (const line of list)
                    this.push(line)

                callback()
            },

            flush(this: LineTransform, callback: TransformCallback) {
                this.last += this.decoder.end()
                if (this.last)
                    this.push(this.last)
                callback()
            }
        })
    }
}

class JsonTransform extends Transform {
    private decoder = new StringDecoder('utf8')

    constructor() {
        super({
            objectMode: true,

            transform(this: JsonTransform, chunk: any, encoding: string, callback: TransformCallback) {
                this.push(JSON.parse(this.decoder.write(chunk)))
                callback()
            }
        })
    }
}

export class GoTestAdapter implements TestAdapter {
	private disposables: { dispose(): void }[] = [];

	private readonly testsEmitter = new vscode.EventEmitter<TestLoadStartedEvent | TestLoadFinishedEvent>();
	private readonly testStatesEmitter = new vscode.EventEmitter<TestRunStartedEvent | TestRunFinishedEvent | TestSuiteEvent | TestEvent>();
    private readonly retireEmitter = new vscode.EventEmitter<RetireEvent>();

	get tests(): vscode.Event<TestLoadStartedEvent | TestLoadFinishedEvent> { return this.testsEmitter.event }
	get testStates(): vscode.Event<TestRunStartedEvent | TestRunFinishedEvent | TestSuiteEvent | TestEvent> { return this.testStatesEmitter.event }
    get retire(): vscode.Event<RetireEvent> | undefined { return this.retireEmitter.event }

    private running: ChildProcess[] = [];
    private collector = new TestCollector();

    private get config() { return Configuration.get(this.workspace.uri) }

	constructor(
		public readonly workspace: vscode.WorkspaceFolder,
		private readonly log: Log
	) {
		this.log.info('Initializing Go test adapter')

        const sub = Configuration.onDidChange(async () => await this.load())

		this.disposables.push(sub, this.testsEmitter, this.testStatesEmitter, this.retireEmitter)

        this.watch()
    }

    async resolve(uriOrGlob: string | vscode.Uri | vscode.Uri[], retire: Set<TestSymbol>, retireWithoutChanges = false): Promise<void> {
        if (typeof uriOrGlob === 'string') {
            const files = await vscode.workspace.findFiles(new vscode.RelativePattern(this.workspace, uriOrGlob))
            await this.resolve(files, retire, retireWithoutChanges)
            return
        }

        if (uriOrGlob instanceof Array) {
            await Promise.all(uriOrGlob.map(x => this.resolve(x, retire, retireWithoutChanges)))
            return
        }

        const file = await File.resolve(uriOrGlob, this.workspace)
        if (!file.pkg.isValid)
            return

        await this.collector.add(file, retire, retireWithoutChanges)
    }

    async load(uri?: vscode.Uri): Promise<void> {
        if (uri && path.basename(uri.fsPath) == 'go.mod') {
            Module.invalidate(uri.with({ path: path.dirname(uri.fsPath) }))
            return
        }

        this.testsEmitter.fire(<TestLoadStartedEvent>{ type: 'started' })

        const goExtension = vscode.extensions.getExtension('golang.go') || vscode.extensions.getExtension('ms-vscode.Go')
        if (!goExtension) {
            this.testsEmitter.fire(<TestLoadFinishedEvent>{ type: 'finished', errorMessage: `Failed to locate the Go extension` })
            return
        }

        try {
            const retire = new Set<TestSymbol>()

            if (!uri) {
                await this.resolve('**/*_test.go', retire)

            } else if (uri.path.endsWith('_test.go')) {
                await this.resolve(uri, retire)

            } else {
                const file = await File.resolve(uri, this.workspace)
                if (!file.pkg.isValid) {
                    this.testsEmitter.fire(<TestLoadFinishedEvent>{ type: 'finished', errorMessage: `File not in workspace: ${uri.toString()}` })
                    return
                }

                await this.resolve(`${file.pkg.name || '.'}/*_test.go`, retire, true)
            }

            for (const pkgName in this.collector.packages)
                if (!this.collector.packages[pkgName].children.some(x => x.children.length))
                    delete this.collector.packages[pkgName]

            const mod = await Module.resolve(this.workspace.uri)
            const suite = this.collector.toInfo(mod?.name || this.workspace.name)
            if (suite.children.length == 0)
                this.testsEmitter.fire(<TestLoadFinishedEvent>{ type: 'finished' })
            else
                this.testsEmitter.fire(<TestLoadFinishedEvent>{ type: 'finished', suite })
            this.retireEmitter.fire(<RetireEvent>{ tests: Array.from(retire).map(x => x.symbol.id) })
        } catch (error) {
            this.testsEmitter.fire(<TestLoadFinishedEvent>{ type: 'finished', errorMessage: `${error}` })
            return
        }
    }

    async run(ids: string[]): Promise<void> {
        this.testStatesEmitter.fire(<TestRunStartedEvent>{ type: 'started', tests: ids })

        try {
            const runs = this.resolveIDs(ids)
            await this.execute(runs)
        } catch (error) {
            this.log.error(error)
        }

		this.testStatesEmitter.fire(<TestRunFinishedEvent>{ type: 'finished' })
    }

    async debug(ids: string[]): Promise<void> {
        const notFound = 'The specified test could not be found'
        const tooMany = 'Multiple test debugging sessions cannot be launched simultaneously'
        const err = (m: string) => (vscode.window.showErrorMessage(m), void 0)

        const runs = this.resolveIDs(ids)
        if (runs.length == 0) return err(notFound)
        if (runs.length != 1) return err(tooMany)

        const run = runs[0]
        if (run.all.length == 0) return err(notFound)
        if (run.all.length != 1) return err(tooMany)

        const test = run.all[0]
        const env = this.env(false)

        const args = test.type == TestType.Benchmark
            ? ['-test.v', '-test.bench', `^${test.symbol.name}$`, '-test.run', 'a^']
            : ['-test.v', '-test.run', `^${test.symbol.name}$`]

        const buildFlags = this.flags({ run: 1 })
        if (buildFlags.indexOf('-args') >= 0) {
            const i = buildFlags.indexOf('-args')
            args.push(buildFlags[i+1])
            buildFlags.splice(i, 2)
        }

        const tags = this.tags()
        if (tags)
            buildFlags.unshift('-tags', tags)

        const name = `Debug ${test.symbol.name} (${run.pkg.name})`
        const didStart = p(vscode.debug.onDidStartDebugSession).call<vscode.DebugSession>((e, r) => { if (e.configuration.name == name) r(e) })
        const didTerminate = p(vscode.debug.onDidTerminateDebugSession).call<vscode.DebugSession>((e, r) => { if (e.configuration.name == name) r(e) })

        this.testStatesEmitter.fire(<TestRunStartedEvent>{ type: 'started', tests: ids })

        const ok = await vscode.debug.startDebugging(this.workspace, {
            name,
            type: 'go',
            request: 'launch',
            mode: 'auto',
            program: test.symbol.file.uri.fsPath,
            env: env,
            args,
            buildFlags: buildFlags.join(' ')
        })
        if (!ok) {
            this.testStatesEmitter.fire(<TestRunFinishedEvent>{ type: 'finished' })
            return err(`Failed to start debugging of ${test.symbol.name} (${run.pkg.name})`)
        }

        const timeout = new Promise<undefined>(r => setTimeout(r, 1000))
        if (!await Promise.race([didStart, timeout])) {
            return err(`Timed out while waiting for debug session for ${test.symbol.name} (${run.pkg.name})`)
        }

        await didTerminate

        this.testStatesEmitter.fire(<TestRunFinishedEvent>{ type: 'finished' })

        function p<T>(event: vscode.Event<T>) {
            return {
                call<V>(listener: (e: T, r: (v: V) => void) => void) {
                    return new Promise<V>(r => {
                        let done = false
                        const sub = event(e => {
                            listener(e, v => {
                                if (done) return
                                done = true
                                sub.dispose()
                                r(v)
                            })
                        })
                    })
                }
            }
        }
    }

    async profile(type: 'cpu' | 'memory', document: vscode.TextDocument, functionName: string): Promise<void> {
        const { run, sym } = (() => {
            for (const pkg of this.collector.children)
                for (const file of pkg.children)
                    if (file.file.uri.toString() == document.uri.toString())
                        for (const sym of file.children)
                            if (sym.symbol.name == functionName) {
                                const run = new TestRun(pkg, this.log, this.testStatesEmitter)
                                run.add(sym)
                                return { run, sym }
                            }
            return {}
        })()
        if (!run || !sym) return

        const exe = await getTempFilePath('test.exe')
        const [flag, file] = type == 'cpu'
            ? ['-cpuprofile', await getTempFilePath('cpu.prof')]
            : ['-memprofile', await getTempFilePath('mem.prof')]

        await this.execute([run], [flag, file, '-o', exe])

        const status = run.isDone(`${sym.id}`)
        switch (status) {
        case 'skipped':
            vscode.window.showErrorMessage(`${functionName} was skipped`)
            return

        case 'failed':
            vscode.window.showErrorMessage(`${functionName} failed`)
            return

        case false:
            vscode.window.showErrorMessage(`${functionName} did not complete`)
            return

        default:
            vscode.window.showErrorMessage(`An unknown error occurred while executing ${functionName}`)
            return

        case 'passed':
            break
        }

        const { error, stdout, stderr } = await new Promise(r => exec(`go tool pprof -svg ${file}`, (error, stdout, stderr) => r({error, stdout, stderr})))
        if (error) {
            const message = error.message.replace(file, () => `$TMP/${type == 'cpu' ? 'cpu' : 'mem'}.prof`)
            vscode.window.showErrorMessage(`Failed to execute \`go tool pprof\`: ${message}`)
            return
        }

        if (stderr)
            vscode.window.showWarningMessage(`go tool pprof: ${stderr}`)

        const svgStart = stdout.indexOf('<svg ')
        const scriptStart = stdout.indexOf('<script ')
        const scriptEnd = stdout.indexOf('</script>')
        if (svgStart < 0 || scriptStart < 0 || scriptEnd < 0) {
            vscode.window.showErrorMessage('Unexpected output from `go tool pprof`')
            return
        }

        const panel = vscode.window.createWebviewPanel(
            `vscode-go-test-adapter-${type}-profile`,
            `${functionName} ${type == 'cpu' ? 'CPU' : 'Memory'} Profile`,
            vscode.ViewColumn.Active,
            {
                enableScripts: true,
            }
        )
        this.disposables.push(panel)

        panel.webview.html = `<html>
            <head>
                <style>
                    html {
                        height: 100%;
                    }

                    body {
                        height: calc(100% - 3px);
                        background: white;
                        padding: 0;
                    }
                </style>
            </head>
            <body>
                <svg id="root" ${stdout.slice(svgStart + 5, scriptStart)}${stdout.slice(scriptEnd+9)}
                ${stdout.slice(scriptStart, scriptEnd + 9).replace(/<!\[CDATA\[|\]\]>/g, '').replace(/"(handle\w+)\(evt\)"/g, '"$1(event)"')}
                <script>
                    setTimeout(() => {
                        window.root = document.body.getElementsByTagName('svg').root
                        window.addEventListener('wheel', handleMouseWheel)
                    }, 0)
                </script>
            </body>
        </html>`
    }

    cancel(): void {
        this.running.forEach(x => x.kill('SIGKILL'))
    }

    dispose(): void {
        this.cancel()

        const { disposables } = this
        this.disposables = []

        for (const d of disposables)
            d.dispose()
    }

    private watch(): void {
        const watcher = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(this.workspace, '**/{*.go,go.mod}'))
        this.disposables.push(watcher)

        this.disposables.push(watcher.onDidCreate(e => this.load(e), null, this.disposables))
        this.disposables.push(watcher.onDidChange(e => this.load(e), null, this.disposables))
        this.disposables.push(watcher.onDidDelete(e => this.load(e), null, this.disposables))
    }

    private resolveIDs(ids: string[]): TestRun[] {
        const runs: Lookup<TestRun> = {}
        for (const id of ids) {
            const [adapter, pkg, file, name] = id.split('·')

            if (adapter != 'Go') {
                this.log.warn(`cannot execute: expected 'Go[·<package>[·<test>]]', got '${id}'`)
                continue
            }

            if (!pkg) {
                for (const testPkg of this.collector.children) {
                    if (!(testPkg.id in runs))
                        runs[testPkg.id] = new TestRun(testPkg, this.log, this.testStatesEmitter)

                    const run = runs[testPkg.id]
                    run.tests = testPkg
                    run.suites.add(testPkg)
                }
                continue
            }

            const testPkg = this.collector.find(pkg)
            if (!testPkg) {
                this.log.warn(`cannot find ID ${id}`)
                this.testStatesEmitter.fire(<TestEvent>{ type: 'test', test: id, state: 'errored' })
                continue
            }

            if (!(testPkg.id in runs))
                runs[testPkg.id] = new TestRun(testPkg, this.log, this.testStatesEmitter)

            const run = runs[testPkg.id]
            if (!file) {
                run.tests = testPkg
                run.suites.add(testPkg)
                continue
            }

            if (run.tests instanceof TestPackage)
                continue

            const testFile = testPkg.find(file)
            if (!testFile) {
                this.log.warn(`cannot find ID ${id}`)
                this.testStatesEmitter.fire(<TestEvent>{ type: 'test', test: id, state: 'errored' })
                continue
            }

            if (!name) {
                testFile.children.forEach(x => run.add(x))
                run.suites.add(testFile)
                continue
            }

            const testSym = testFile.find(name)
            if (!testSym) {
                this.log.warn(`cannot find ID ${id}`)
                this.testStatesEmitter.fire(<TestEvent>{ type: 'test', test: id, state: 'errored' })
                continue
            }

            run.add(testSym)
        }

        return Object.values(runs)
    }

    private async execute(runs: TestRun[], extraArgs: string[] = []): Promise<void> {
        const tags = this.tags()
        const flags = this.flags({ run: 1 })
        const env = this.env()
        const bin = this.bin()
        const baseArgs = ['test', '-json', ...extraArgs]
        const goConfig = getGoConfig();

        if (tags && flags.indexOf('-tags') < 0)
            baseArgs.push('-tags', tags)

        baseArgs.push(...flags)

        // run tests in parallel
        await Promise.all(runs.map(async run => {
            if (!run.hasTests && !run.hasBenchmarks)
                return

            const benchmarks: TestSymbol[] = []
            if (run.tests instanceof Set)
                benchmarks.push(...run.benchmarks)
            else if (!this.config.runPackage.excludeBenchmarks)
                for (const file of run.tests.children)
                    for (const sym of file.children)
                        if (sym.type == TestType.Benchmark)
                            benchmarks.push(sym)

            const args = [...baseArgs]

            if (run.hasTests) {
                args.push('-timeout', `${goConfig.testTimeout}`)
                if (run.tests instanceof Set)
                    args.push('-run', `^(${[...run.tests].map(x => x.symbol.name).join('|')})$`)
            }

            if (benchmarks.length) {
                args.push('-benchmem', '-bench')
                if (run.tests instanceof Set)
                    args.push(`^(${[...run.benchmarks].map(x => x.symbol.name).join('|')})$`)
                else
                    args.push('.')
                if (!run.hasTests)
                    args.push('-run', 'NONE')
            }

            run.fireAllSuites({ state: 'running' })

            if (benchmarks.length)
                run.fireBenchmarks('running')

            const cwd = run.pkg.uri.fsPath
            const { code, signal, errors } = await this.spawn(bin, args, env, cwd, e => run.event(e))

            if ((code || signal) && run.tests instanceof Set) {
                const state = 'errored'
                const message = `${signal ? 'go test received a signal to terminate' : 'go test exited with a non-zero code'}\n\nOutput:\n${errors.join('\n')}`

                for (const test of run.tests)
                    if (!run.isDone(test.id))
                        this.testStatesEmitter.fire(<TestEvent>{ type: 'test', test: test.id, state, message })
            }

            if (benchmarks.length)
                if (signal)
                    run.fireBenchmarks('errored', 'go test received a signal to terminate')
                else if (code)
                    run.fireBenchmarks('failed')
                else
                    run.fireBenchmarks('passed')

            run.fireAllSuites({ state: 'completed' })
        }))
    }

    async spawn(bin: string, args: string[], env: Lookup<string | undefined>, cwd: string, callback: (e: any) => void) {
        const proc = spawn(bin, args, { env, cwd })

        const stdout = proc.stdout
            .pipe(new LineTransform)
            .pipe(new JsonTransform)
            .on('data', callback)
            .on('error', e => this.log.error(`An error occurred while processing stdout:`, e))

        const errors: string[] = []
        const stderr = proc.stderr
            .pipe(new LineTransform)
            .on('data', (event: string) => (this.log.error(event), errors.push(event)))
            .on('error', e => this.log.error(`An error occurred while processing stderr:`, e))

        const { code, signal } = await new Promise(resolve => {
            this.running.push(proc)
            proc.on('close', (code, signal) => resolve({ code, signal }))
        })

        const index = this.running.indexOf(proc)
        if (index >= 0)
            this.running.splice(index, 1)

        stdout.destroy()
        stderr.destroy()

        return { code, signal, errors }
    }

    private tags(): string | undefined {
        return getTestTags(getGoConfig())
    }

    private flags(omit: { [key: string]: number } = {}): string[] {
        const flags = getTestFlags(getGoConfig())

        for (const name in omit) {
            const i = flags.indexOf(`-${name}`)
            if (i >= 0) flags.splice(i, omit[name]+1)
        }

        return flags
    }

    private env(proc = true): {[key: string]: string | undefined} {
        const env: any = {}
        if (proc)
            Object.assign(env, process.env)

        Object.assign(env, getTestEnvVars(getGoConfig()))

        return env
    }

    private bin(): string {
        return getBinPath('go')
    }
}

async function exists(file: string): Promise<boolean> {
    try {
        await fsp.stat(file)
        return true
    } catch (error) {
        if (error.code && error.code == 'ENOENT')
            return false
        throw error
    }
}

let tmpDir: string | undefined;
export async function getTempFilePath(name: string): Promise<string> {
	if (!tmpDir)
		tmpDir = await fsp.mkdtemp(os.tmpdir() + path.sep + 'vscode-go-test-adapter');

	if (!await exists(tmpDir))
		await fsp.mkdir(tmpDir)

	return path.normalize(path.join(tmpDir!, name));
}

export async function deleteTempDir() {
    if (!tmpDir) return
    if (!await exists(tmpDir)) return

    await rm(tmpDir)

    async function rm(dir: string) {
        const files = await fsp.readdir(dir)
        await Promise.all(files.map(async (name: string) => {
            const p = path.join(dir, name)
            const stat = await fsp.lstat(p)
            if (stat.isDirectory())
                await rm(p)
            else
                await fsp.unlink(p)
        }))
    }
}